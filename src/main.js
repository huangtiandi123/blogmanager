// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import ElementUI from 'element-ui'
import 'element-ui/lib/theme-chalk/index.css'
import 'element-ui/lib/theme-chalk/display.css'
import { getCookie } from './utils/cookie'
import App from './App'
import VueRouter from 'vue-router'
import routes from './router/index'
import './assets/style/style.css'
// import mavonEditor from 'mavon-editor'
// import 'mavon-editor/dist/css/index.css'

import hljs from 'highlight.js' //导入代码高亮文件
import 'highlight.js/styles/monokai-sublime.css'  //导入代码高亮样式
//自定义一个代码高亮指令
Vue.directive('highlight',function (el) {
  let highlight = el.querySelectorAll('pre code');
  highlight.forEach((block)=>{
      hljs.highlightBlock(block)
  })
})

Vue.config.productionTip = false

Vue.use(ElementUI);
Vue.use(VueRouter);
// Vue.use(VueQuillEditor);
// use
// Vue.use(mavonEditor)


const router = new VueRouter({
  linkActiveClass: 'is-active',
  routes,
  mode: 'history'
});
// 使用钩子函数对路由进行权限跳转
router.beforeEach((to, from, next) => {
  // localStorage  本地存储
  const roles = localStorage.getItem('roles');
  // const permissions = localStorage.getItem('permissions');
  let cookieroles = getCookie('roles'); // 取cookie角色
  console.log('输出cookie', cookieroles);
  if(!cookieroles && to.path !== '/login'){  // 判断路径是否login, cookie有登录用户信息跳转页面
    next({path: '/login'});
  }else if(to.meta.permission){
    roles.indexOf('admin') > -1 ? next() : next({path:'/403'});
  }else{
    next();
  }

})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  components: { App },
  template: '<App/>'
})

// import axios from "axios"

// axios.post('http://localhost:8081/admin/api/addarticle',
//   {
//     articleTitle:'我的测试',
//     description:'简介',
//     articleDate:'2020-12-22'
//   }).then(res =>{
//   console.log(res.data);
// }).catch(function(error){
//   console.log("error: ",error);
// })