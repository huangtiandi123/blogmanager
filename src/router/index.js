const routes = [ // 一级路由
    {
        path: '/login',
        name: 'Login',
        component: resolve => require(['../pages/Login'], resolve),
    },
    {
        path: '/',
        name: 'Index',
        component: resolve => require(['../pages/Index'], resolve),
        meta: { permission: true }, // 是否配置权限
        children: [{
            path: '/relaedinfo',
            name: 'relaedinfo',
            component: resolve => require(['../components/panels/Relaedinfo'], resolve),
        }, { //二级路由
            path: '/artmess',
            name: 'Artmess',
            component: resolve => require(['../components/panels/Artmess'], resolve),
        }, {
            path: '/articlemg',
            name: 'ArticleMg',
            component: resolve => require(['../components/panels/ArticleMg'], resolve),
            meta: ['文章管理', '文章管理']
        }, {
            path: '/newarticle',
            name: 'NewArticle',
            component: resolve => require(['../components/panels/NewArticle'], resolve),
            meta: ['文章管理', '文章查看 / 编辑 / 删除']
        }, {
            path: '/comments',
            name: 'Comments',
            component: resolve => require(['../components/panels/Comments'], resolve),
            meta: ['文章管理', '评论审核']
        }, {
            path: '/leav',
            name: 'leav',
            component: resolve => require(['../components/panels/Leav'], resolve),
            meta: ['文章管理', '留言管理']
        }, {
            path: '/videos',
            name: 'videos',
            component: resolve => require(['../components/panels/Videos'], resolve),
            meta: ['文章管理', '视频管理']
        }],
        redirect: '/relaedinfo' // 默认显示子组件
    }, {
        path: '/403',
        component: resolve => require(['../components/public/403'], resolve),
        meta: { title: '403' }

    }, {
        path: '*',
        redirect: '/'
    }
]

export default routes;