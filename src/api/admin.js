import request from '../utils/request.js'

export function userLogin(params){
    return request({
        url: "/admin/login",
        method: "post",
        data:params
    })
}

export function getComment(){
    return request({
        url: "/api/comment/allunrevieded",
        method: "get",
    })
}

export function upCheck(params){
    return request({
        url: "/api/comment/checkoper",
        method: "post",
        data:params
    })
}