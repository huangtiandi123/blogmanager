import request from '../utils/request.js'

export function getArticleList(params){
    return request({
        // http://localhost:8081
        url:"/api/article/list",
        method: "get",
        params: params
    })
}

export function getArticleInfor(params){
    return request({
        url: "/api/article/" + params.id + ".html",
        method: "get"
        // params: params
    })
}

export function addArticle(params){
    return request({
        url: "/admin/api/addarticle",
        method: 'post',
        data:params
    });
}