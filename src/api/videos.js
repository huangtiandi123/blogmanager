import request from '../utils/request.js'

export function addVideo(params){
    return request({
        url: '/api/videos/add',
        method: 'post',
        data: params
    })
}