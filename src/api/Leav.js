import request from '../utils/request'

export function getLeavList(){
    return request({
        url: '/api/mess/list',
        method: 'get'
    })
}

export function sendMail(params){
    return request({
        url: '/api/mess/sendmail',
        method: 'post',
        data: params
    })
}