
const getters = {
    collapse: (state) => state.collapse,
  }

const actions = {
changeCollapse({ commit }) {
    commit('changeCollapse')
}
}